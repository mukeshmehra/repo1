import React,{useState,useEffect} from 'react'
import { useRouter } from 'next/router';
import axios from 'axios';
import styles from './Product.module.css';
import Image from 'next/image';
const ProductDetail = () => {
    const router = useRouter();
    const [product,setProduct] = useState(null)
    useEffect(()=>{
      const {id}= router.query;
      axios.post('/api/product',{
        id: id,
        }).then(resp=>{
        setProduct(resp.data.product);
       }).catch(error=>console.error(error)).finally(()=>{
       })
    },[router])   
  return (
  <>
  {product&& 
      <div className={styles.grid}>
        <Image className={styles.image} width={500} height={400} src={product.image} objectFit="contain"/>
        <div className={styles.detail}>
          <p className={styles.productName}>{product.title}</p>
          <p className={styles.category}>{product.category}</p>
          <p className={styles.price}>&#x20b9; {product.price}</p>
          <p className={styles.description}>{product.description}</p>
          <div style={{display:"flex"}}> 
            <button>Add to Cart</button>
            <button onClick={()=>{
              router.back();
            }}>Go Back</button>
          </div>
        </div>
        
      </div>
    }
  </>
  )
}

export default ProductDetail