import React,{ useEffect,useState } from 'react';
import axios from 'axios';
import styles from './Products.module.css';
import Ratings from '../../components/ratings';
import {useRouter} from 'next/router';
import Image from 'next/image';
const Products = () => {
    const [products,setProducts] = useState([])
    useEffect(()=>{
       axios.get('api/products').then(resp=>{
        setProducts(resp.data.products);
       }).catch(error=>console.error(error)).finally(()=>{
       })
    },[])

    const router = useRouter();
    const handleProductClick=(id)=>{
        router.push(`/product/${id}`);
    }
  return (
      <>
      <div className="container">
        <div className={styles.grid}>
            {products && products.map((item)=>
                <div className={styles.card} key={item.id} onClick={()=>handleProductClick(item.id)}>
                        <Image className={styles.image} width={150} height={180} src={item.image} objectFit="contain"/>
                        <Ratings starCount={item.rating?.rate}></Ratings>{item.rating?.count}
                        <p className={styles.productName}>{item.title}</p>
                        <p className={styles.price}>&#x20b9; {item.price}</p>
                </div>)
            }
        </div>
      </div>
        
      </>
  )
}

export default Products