import '../styles/globals.css'
import Layout from '../components/navbar';

function MyApp({ Component, pageProps }) {
  return <Layout>
            <Component {...pageProps}></Component>
          </Layout>
}

export default MyApp
