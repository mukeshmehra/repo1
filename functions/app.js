// Require the driver
require('dotenv').config();
const axios = require('axios');
// Acquire the secret and optional endpoint from environment variables
const secret = process.env.FAUNADB_SECRET
var endpoint = process.env.FAUNADB_ENDPOINT

exports.handler = async function (event, context) {
    const GET_CUSTOMERS=
    `query FindAllCustomers{
        allCustomers{
          data{
            _id
            firstName
            telephone
          }
        }
      }`;

    if (typeof secret === 'undefined' || secret === '') {
        console.error('The FAUNADB_SECRET environment variable is not set, exiting.')
        process.exit(1)
    }

    const {data}=await axios({
        url:'https://graphql.fauna.com/graphql',
        method:'POST',
        headers:{
            Authorization:`Bearer ${secret}`
        },
        data:{
            query:GET_CUSTOMERS,
            variables:{}
        }
    })
    console.log(data);
    return {
      statusCode: 200,
      body: JSON.stringify(data),
    };
  }