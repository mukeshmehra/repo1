import styles from "./Layout.module.css";
import { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import logo from '../public/images/logo.png'
import {useRouter} from 'next/router';
import { useEffect } from "react/cjs/react.development";

export default function Layout({children}){
  const menus=[ {path:"/portfolio", name:'Portfolio'},
  {path:"/product", name:'Shop'},
  {path:"/", name:'About'},
  {path:"/contact", name:'Contact Us'},
  ];
  const [isOpen,setOpen] = useState(false);
  const [activePath,setActivePath]=  useState('');
  const toggleMenu = ()=>{
      setOpen(!isOpen);
  }
  const closeMenu=()=>{
    setOpen(false);
  }

  const router = useRouter();
  useEffect(()=>{
    if(router.pathname.indexOf("product")!==-1){
      setActivePath("/product");
    }else{
      setActivePath(router.pathname);
    }
    
  },[router.pathname])
    return <>
        <nav className={styles.navbar}>
        <Image
        src={logo}
        alt="My Logo MM"
        className={styles.brandTitle}
      />
        <a href="#" className={styles.toggleButton} onClick={toggleMenu}>
          <span className={styles.bar}></span>
          <span className={styles.bar}></span>
          <span className={styles.bar}></span>
        </a>
        <div className={isOpen?styles.navbarLinks +' '+styles.visible:styles.navbarLinks +' '+styles.hidden}>
          <ul>
            {
              menus.map(
                (item)=> (
                <li key={item.name} className={activePath===item.path?styles.selected:''} 
                  onClick={closeMenu}> 
                <Link href={item.path}><a>{item.name}</a></Link> 
                </li>)
              )}       
          </ul>
        </div>
      </nav>
        {children}  
    </>
}