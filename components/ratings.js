import React from 'react'
const Ratings = ({starCount}) => {
    starCount=Math.round(+starCount);
    const stars=[];
    for (let star=0;star<5;star++){
        if(star<starCount-1){
            stars.push(<span key={star} className="fa fa-star starCheked"></span>)
        }else{
            stars.push(<span key={star} className="fa fa-star starNormal"></span>)
        }
    }
  return (
      <div>{stars}</div>
  )
}

export default Ratings;