module.exports = {
  reactStrictMode: true,
  swcMinify: false,
  distDir: 'build',
  images: {
    domains: ['fakestoreapi.com'],
  },

  async redirects() {
    return [
      {
        source: '/',
        destination: '/product/',
        permanent: true,
      },
    ]
  }
}
